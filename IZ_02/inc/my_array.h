#ifndef INC_MY_ARRAY_H_
#define INC_MY_ARRAY_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ALLOC_ERR -201

// Расширение массива при его заполнении
int realloc_by_len(void **arr, size_t elem_size, size_t arr_len, size_t *buf_len);
// Вставка в конец массива с возможным расширением
int insert_to_array(void **arr, void *elem, size_t elem_size, size_t arr_len, size_t *buf_len);

#endif  // INC_MY_ARRAY_H_
