#ifndef INC_MY_STRUCT_H_
#define INC_MY_STRUCT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_array.h"

enum my_struct_error {
    struct_error,
    STRUCT_READ_ERROR,
    STRUCT_VALID_ERROR,
    STRUCT_FIELD_NOT_FOUND};

struct rating_s {
    int id;
    int score_count;
    float score;
};
typedef struct rating_s struct_t;

void print_struct(struct_t elem);
void print_structs(struct_t elems[], size_t len);
int read_struct_stream(FILE *stream, struct_t *elem);

#endif // INC_MY_STRUCT_H_
