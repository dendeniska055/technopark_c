#ifndef INC_WORK_H_
#define INC_WORK_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <time.h>

#include <dlfcn.h>

#include "my_array.h"
#include "my_struct.h"
#include "dinamic.h"

enum work_error {
    work_error,
    FILE_READ_ERROR,
    INPUT_ERROR,
};

#define FALSE 0
#define TRUE 1

#define DEFULT_ARR_LEN 5
#define TEST_COUNT 20

int read_file(const char *filename, struct_t **containers, size_t *len, size_t *buf_len);
long get_zero_count(struct_t *arr, size_t len, size_t process_count);
void speed_test(size_t speed_test_len, size_t process_count);

#endif  // INC_WORK_H_
