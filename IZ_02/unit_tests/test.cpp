#include "gtest/gtest.h"

extern "C"
{
#include "../inc/my_array.h"
#include "../inc/my_struct.h"
#include "../inc/work.h"
}

TEST(insert_to_arrayTest, AddElem)
{
    int *arr = reinterpret_cast<int *>(calloc(5, sizeof(int)));
    arr[0] = 7;
    int elem = 10;
    int len = 1;
    size_t buf = 5;
    EXPECT_EQ(insert_to_array((void **)&arr, &elem, sizeof(int), len, &buf), 0);

    EXPECT_EQ(buf, 5);

    int tmp_arr[] = {7, 10};
    for (int i = 0; i < 2; ++i)
    {
        EXPECT_EQ(arr[i], tmp_arr[i]);
    }
    free(arr);
}

TEST(get_zero_count, Test_0_len)
{
    size_t len = 0, process_count = 1;
    struct_t *arr = reinterpret_cast<struct_t *>(calloc(len, sizeof(struct_t)));
//    arr[0] = {1, 0, 2.0};
//    arr[1] = {2, 1, 3.0};
//    arr[2] = {3, 0, 4.0};
    EXPECT_EQ(get_zero_count(arr, len, process_count), 0);
    free(arr);
}

TEST(get_zero_count, Test1)
{
    size_t len = 3, process_count = 1;
    struct_t *arr = reinterpret_cast<struct_t *>(calloc(len, sizeof(struct_t)));

    arr[0] = {1, 0, 2.0};
    arr[1] = {2, 1, 3.0};
    arr[2] = {3, 0, 4.0};

    EXPECT_EQ(get_zero_count(arr, len, process_count), 2);
    free(arr);
}

TEST(get_zero_count, Test_all)
{
    size_t len = 3, process_count = 1;
    struct_t *arr = reinterpret_cast<struct_t *>(calloc(len, sizeof(struct_t)));

    arr[0] = {1, 0, 2.0};
    arr[1] = {2, 0, 3.0};
    arr[2] = {3, 0, 4.0};

    EXPECT_EQ(get_zero_count(arr, len, process_count), 3);
    free(arr);
}

TEST(get_zero_count, Test_nothin)
{
    size_t len = 3, process_count = 1;
    struct_t *arr = reinterpret_cast<struct_t *>(calloc(len, sizeof(struct_t)));

    arr[0] = {1, 1, 2.0};
    arr[1] = {2, 1, 3.0};
    arr[2] = {3, 1, 4.0};

    EXPECT_EQ(get_zero_count(arr, len, process_count), 0);
    free(arr);
}

TEST(get_zero_count, Test_many)
{
    size_t len = 6, process_count = 1;
    struct_t *arr = reinterpret_cast<struct_t *>(calloc(len, sizeof(struct_t)));

    arr[0] = {1, 0, 2.0};
    arr[1] = {2, 1, 3.0};
    arr[2] = {3, 0, 4.0};
    arr[3] = {1, 0, 2.0};
    arr[4] = {2, 1, 3.0};
    arr[5] = {3, 0, 4.0};

    EXPECT_EQ(get_zero_count(arr, len, process_count), 4);
    free(arr);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
