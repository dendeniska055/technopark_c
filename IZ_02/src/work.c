#include "work.h"

int read_file(const char *filename, struct_t **ratings, size_t *len, size_t *buf_len)
{
    int err = 0, end_input = 0;

    FILE *stream = stdin;
    if (strlen(filename) > 0)
    {
        printf("%s\n", filename);
        stream = fopen(filename, "r");
        if (!stream)
        {
            printf("Error read file!\n");
            return FILE_READ_ERROR;
        }
    }
    else
    {
        if (fscanf(stream, "%ld", len) != 1)
            return STRUCT_READ_ERROR;
    }

    struct_t rating;
    size_t arr_len = 0;
    while (!err && !end_input && (arr_len < *len || stream != stdin))
    {
        err = read_struct_stream(stream, &rating);
        if (!err)
            err = insert_to_array((void **)ratings, &rating, sizeof(struct rating_s), arr_len, buf_len);
        arr_len++;

        if (feof(stream) || (arr_len >= *len && stream == stdin))
        {
            end_input = TRUE;
        }
    }
    *len = arr_len;
    if (strlen(filename) > 0)
        fclose(stream);
    return err;
}

long get_zero_count_async(struct_t *arr, size_t len)
{
    long count = 0;
    for (size_t i = 0; i < len; i++)
        if (arr[i].score_count == 0)
            count++;
    return count;
}

long get_zero_count(struct_t *arr, size_t len, size_t process_count)
{
    if (process_count == 1)
    {
        return get_zero_count_async(arr, len);
    }
    else if (process_count > 1)
    {
        void *dynlib_worker = dlopen("./libdinamic.so", RTLD_LAZY);
        if (!dynlib_worker)
            return 0;

        long (*get_zero_count_sync)(struct_t *arr, size_t len, size_t process_count) =
            dlsym(dynlib_worker, "get_zero_count_sync");

        return get_zero_count_sync(arr, len, process_count);
    }
    else
        return 0;
}

void speed_test_func(struct_t *arr, size_t len, size_t process_count)
{
    unsigned long long elapsed_time;
    long count = 0;

    elapsed_time = clock();
    for (size_t i = 0; i < TEST_COUNT; i++)
        count = get_zero_count(arr, len, process_count);
    elapsed_time = clock() - elapsed_time;

    printf("process_count: %ld, time:%lf\n",
           process_count, (double)elapsed_time / (double)TEST_COUNT);
}

void speed_test(size_t speed_test_len, size_t process_count)
{
    struct_t *ratings = (struct_t *)calloc(speed_test_len, sizeof(struct_t));
    for (size_t i = 0; i < speed_test_len; i++)
    {
        ratings[i].id = i;
        ratings[i].score = 2.0;
        ratings[i].score_count = i % 2;
    }
    printf("len: %ld, TEST_COUNT: %d\n", speed_test_len, TEST_COUNT);
    for (size_t i = 1; i <= process_count; i++)
        speed_test_func(ratings, speed_test_len, i);
    free(ratings);
}